-- SUMMARY --

NVP field allows you to create a field type of name/value pairs, with custom
titles and easily editable rendering.

This allows you to easily create content blocks of with pairs within divs, list
items, data description lists, or in your own custom plain text.

In addition, customising of the titles for LHS and RHS values allows you to
customise the field to the specific field, rather than a generic "val1" and
"val2"

i.e.
<div class="nvppair name">Director</div> \
<div class="nvppair value">Dylan Thomas</div>
<div class="nvppair name">Actor</div> \
<div class="nvppair value">George Formby</div>
<div class="nvppair name">Producer</div> \
<div class="nvppair value">Jaws</div>

-- REQUIREMENTS --

None.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

Each instance of a NVP allows you to configure titles and the rendering.

When creating a content type or adding a field to an existing type, select NVP
as field type.

In field settings:

"Name title": field title the user sees for the entering LHS data.
"Value title": field title the user sees for the entering RHS data.
"Prefix text": HTML code or plain text to be prefixed to the nvp.
"Delimeter text": HTML code or plain text to be placed between the nvp values.
"Suffix text": HTML code or plain text to be suffixed to the nvp.

For the example in summary, config could be:
"Name title": Role
"Value title": Name
"Prefix text": <div class="nvppair name">
"Delimeter text": </div><div class="nvppair value">
"Suffix text": </div>

-- CUSTOMIZATION --

To override the template for the field, and allow custom wrappers to the field,
see http://api.drupal.org/api/drupal/modules%21field%21field.module/function \
/theme_field/7

THEMENAME_field__fieldname()
field-fieldname.tpl.php

-- CONTACT --

Current maintainers:
* John Avery (john_a) - http://drupal.org/user/2573976
* Leon Kelly (leonkelly) - http://drupal.org/user/2574182
